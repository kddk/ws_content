Title: KDDK Positionspapier
Date: 2023-11-23
Author: kddk
save_as: positionspapier.html
Template: page
og_description: 12 Thesen zur Gründung der Initiative zur Förderung einer konstruktiven digitalen Diskussionskultur



# KDDK Positionspapier 1.0 <img src="/theme/img/kddk_logo1.png" style="width: 1em;">
## 12 Thesen zur Gründung der Initiative zur Förderung einer *konstruktiven digitalen Diskussionskultur*

Veröffentlicht: 2023-11-28

**Übersicht/Inhalt:**

!!! pp_content_list ""
    1. [Die Bedeutung von Diskussionen in digitalen Medien nimmt seit Jahren beständig zu.](#these1)
    1. [Über digitale Medien geführte Diskussionen eskalieren tendenziell schneller oder werden ergebnislos abgebrochen.](#these2)
    1. [Die Unfähigkeit, hinreichend diverse und kontroverse Standpunkte in (digitale) Diskussionen einzubeziehen, wirkt sich negativ auf die Problemlösefähigkeit von Gruppen aus. Dies stellt ein Problem für die Demokratie dar.](#these3)
    1. [Existierende Kommunikationsplattformen haben Anreizstrukturen und Eigenschaften, die dysfunktionale Kommunikation begünstigen.](#these4)
    1. [Niedrige Beteiligungsschwellen führen oft zu hohem Nachrichtenaufkommen, niedriger durchschnittlicher Nachrichtenqualität und erzeugen ein Übersichtsproblem.](#these5)
    1. [Konstruktive Diskussionen sind anstrengend.](#these6)
    1. [Für die Verbesserung der digitalen Diskussionskultur braucht es einen Kulturwandel, der über technische Lösungen hinausgeht.](#these7)
    1. [Gruppen, die es schaffen, eine konstruktive (digitale) Diskussionskultur zu etablieren, haben erhebliche Vorteile.](#these8)
    1. [Anreizstrukturen und Eigenschaften von digitalen Kommunikationsplattformen lassen sich (um)gestalten.](#these9)
    1. [Es gibt bereits eine Reihe technischer und sozialer Ansätze zur Unterstützung konstruktiver Diskussionen, welche aber noch getestet, verbessert und etabliert werden müssen.](#these10)
    1. [Es braucht eine Bewegung, die sich mit dem Thema befasst und aktiv an pragmatischen Lösungen arbeitet, die über Appelle hinausgehen.](#these11)
    1. [Das Zeitfenster schließt sich. Wir müssen jetzt handeln!](#these12)

### Vorbemerkung


Diese Thesen fassen zusammen, warum es die Gruppe *KDDK* gibt, mit welchen Problemen sie sich befasst und wie aus KDDK-Perspektive mögliche Lösungen aussehen.


### Status Quo

#### ① Die Bedeutung von Diskussionen in digitalen Medien nimmt seit Jahren beständig zu. {: #these1}

Während früher Themen und Tonfall von klassischen Medien (Fernsehen, Radio, Print) und synchronen Präsenz-Gesprächen (z. B. an Stammtischen, bei Diskussionsveranstaltungen etc.) prägend für den gesellschaftlichen Diskurs waren, ist seit der flächendeckenden Verbreitung des Internets, des Smartphones und sogenannter "Sozialer Medien" der *digitale Raum* zu einer wichtigen und für viele Menschen sogar zu der wichtigsten Sphäre für die Meinungsbildung geworden.

Im Folgenden wird unter *digitalen Diskussionen* eine textbasierte und meist zeitversetzte Kommunikation verstanden, wie sie z. B. für Kommentarbereiche, Chatgruppen oder Plattformmedien wie X (Twitter), Facebook, aber auch Mastodon etc. üblich ist.

Anmerkung: Für die großen zentralisierten und algorithmisch auf das Ausspielen von personalisierter Werbung hin optimierten Dienste ist die übliche Bezeichnung "Soziale Medien" aus unserer Sicht unangemessen. Im Kontext dieser Thesen wird der Begriff deshalb meist in Anführungszeichen verwendet.


---

#### ② Über digitale Medien geführte Diskussionen eskalieren tendenziell schneller oder werden ergebnislos abgebrochen. {: #these2}

Die Erfahrung zeigt: Die Äußerung von Dissens ist auf digitalen Plattformen oft sehr extrem, inklusive Beleidigung, Bedrohung und allgemeiner Hetze. Andererseits "versanden" viele digitale Diskussionen, d. h. nach dem anfänglichen Austausch von Argumenten wird irgendwann nicht mehr geantwortet. Das bedeutet typischerweise aber nicht, dass eine Seite inhaltlich überzeugen konnte.

---


### Bewertung



#### ③ Die Unfähigkeit, hinreichend diverse und kontroverse Standpunkte in (digitale) Diskussionen einzubeziehen, wirkt sich negativ auf die Problemlösefähigkeit von Gruppen aus. Dies stellt ein Problem für die Demokratie dar. {: #these3}

Wenn Menschen in Gruppen zusammenarbeiten, ist es oft notwendig, bezüglich konkreter Sachfragen Entscheidungen zu treffen. Wenn dies nicht willkürlich oder hierarchisch passieren soll, ist vor der Entscheidungsfindung eine Diskussion über die möglichen Optionen und ihre Vor- und Nachteile unerlässlich. Die Erfahrung zeigt, dass Menschen bezüglich vieler Sachfragen mit teils sehr unterschiedlichen Meinungen an solchen Diskussionen teilnehmen. Im Idealfall könnten in einer gelingenden Diskussion alle Argumente erörtert werden, so dass im Ergebnis sich entweder eine optimale Konsenslösung herauskristallisiert oder zumindest alle Teilnehmenden eine informierte Wahl treffen können und z. B. per Abstimmung entschieden wird.

Ein dysfunktionaler Diskussionsprozess hat hingegen zur Folge, dass Entscheidungen getroffen werden, ohne dass alle dafür relevanten Argumente einbezogen wurden, oder dass Entscheidungen verschleppt werden. Zudem treten als Begleiterscheinung oft noch Konflikte auf menschlicher Ebene auf, zwischen Individuen oder zwischen (Teil-)Gruppen. Solche Konflikte binden teils erhebliche Ressourcen und können zur Lähmung oder Spaltung der ursprünglichen Gruppe führen.


Ähnliche Effekte bestehen auch auf gesamtgesellschaftlicher Ebene, wo eine Vielzahl an (miteinander verknüpften) Sachfragen zu entscheiden ist. Es ist zu beobachten, dass unterschiedliche Teile der Gesellschaft miteinander keine ausreichende Gesprächsgrundlage mehr haben und in ihren jeweils eigenen Debatten von unterschiedlichen Informationslagen ausgehen. Das beeinträchtigt den Austausch von Wissen und Perspektiven und damit die informierte Meinungsbildung. Diese wiederum ist notwendige Voraussetzung für eine Demokratie, in der jede Stimme gleich viel wert ist.

---

### Analyse

#### ④ Existierende Kommunikationsplattformen haben Anreizstrukturen und Eigenschaften, die dysfunktionale Kommunikation begünstigen. {: #these4}

Das Geschäftsmodell vieler Kommunikationsplattformen hängt von möglichst viel "Screentime" der User:innen ab. Daher sind beispielsweise Algorithmen auf Facebook und X (Twitter) darauf optimiert, möglichst viel Interaktion zu erzeugen. Das wird am besten durch das Betonen emotionaler Inhalte erreicht. Dadurch bekommen besonders polarisierende Äußerungen überproportional viel Aufmerksamkeit (völlig unabhängig von ihrer objektiven Korrektheit).
Eine Zeichenbegrenzung in Nachrichten erzwingt zudem oft verkürzte bzw. zugespitzte und mitunter missverständliche Formulierungen.

Auf vielen Kommunikationsplattformen gibt es virtuelle Belohnungen, typischerweise in Form von Zustimmung ("Likes") und Weiterleitung ("Shares"). Das setzt Anreize, Beiträge zu produzieren, die mutmaßlich populär sind und führt tendenziell zu (unbewusster) mentaler Ausrichtung auf Themen, Sichtweisen und Darstellungen, die sich gut verbreiten lassen. Da dieser Effekt grundsätzlich alle Kommunikationsteilnehmer:innen betrifft, wird das Entstehen von "Echokammern" befördert.

Eine weitere Problematik betrifft den Komplex "Anonymität": Anonyme Kommunikationsmöglichkeiten führen erfahrungsgemäß oft zu Desinformation, Beleidigungen und Schlimmerem.

Andererseits erlaubt es Anonymität, Minderheitenpositionen ohne Angst vor Repressionen zu äußern. Gerade im digitalen Raum, wo viele Inhalte dauerhaft gespeichert werden, ist diese Befürchtung besonders berechtigt, weil sich kritische Inhalte in Zukunft (z. B. nach einem möglichen Regierungswechsel) nachteilig auswirken können. Fehlende Anonymität kann somit zu präventiver Selbstzensur führen.

Auch wenn es keine offensichtliche Lösung für diese Problematik gibt,
sollte beim Entwurf zukünftiger Kommunikationslösungen bedacht werden, dass der Grad der Anonymität starke Auswirkungen auf das Ergebnis hat.

---


#### ⑤ Niedrige Beteiligungsschwellen führen oft zu hohem Nachrichtenaufkommen, niedriger durchschnittlicher Nachrichtenqualität und erzeugen ein Übersichtsproblem. {: #these5}

In Gruppenkommunikation tritt oft das Phänomen auf: "Es wurde zwar schon alles gesagt, aber noch nicht von jedem." Im Kommentarbereich zu einem Artikel über ein kontroverses Thema äußert sich das z. B. dadurch, dass innerhalb kurzer Zeit hunderte Kommentare abgegeben werden. Es ist unrealistisch anzunehmen, dass die Kommentierenden im Schnitt mehr als 20 Kommentare lesen, bevor sie selber kommentieren. Im Sinne einer konstruktiven Diskussion ist das problematisch, weil so z. B. Argumente, die bereits geäußert und mit einem Gegenargument konfrontiert wurden, als vermeintlich "neues" Argument in die Debatte eingebracht werden und damit die Gelegenheit verpasst wird, sich mit dem vorgebrachten Gegenargument auseinanderzusetzen. Die Diskussion kommt insgesamt also nicht voran.

Viele Nachrichten (Kommentare unter Medienbeiträgen oder "Social-Media"-Posts) erfüllen primär das legitime Bedürfnis, Emotionen zu äußern, enthalten aber keine inhaltlich relevanten Debattenbeiträge. Werden inhaltlich relevante Kommentare, z. B. mit alternativen Quellen oder Argumenten, auf die gleiche Weise publiziert, gehen sie oft im Rauschen unter, es besteht also ein schlechtes Signal-Rausch-Verhältnis.

---

#### ⑥ Konstruktive Diskussionen sind anstrengend. {: #these6}

Konstruktive Diskussionen erfordern die Bereitschaft, sich in der themenspezifischen Detailtiefe mit Inhalten zu befassen. Das bedeutet z. B. die eigene Position überzeugend zu formulieren (mit Logik, mit Quellen, mit passenden Begriffen) und das kostet Zeit und Mühe. In großen Gruppen ist die dafür erforderliche Geduld oft nicht vorhanden.

Ein weiteres Problem ist die Verlockung, eine Diskussion zu emotionalisieren, z. B. durch bewusstes Missverstehen, den Wechsel auf die persönliche Ebene, das Äußern von (subtilen) Provokationen oder das übermäßige Reagieren darauf. Dieser Verlockung zu widerstehen – und damit von der üblichen digitalen Kommunikationspraxis abzuweichen – bedarf oft einer zusätzlichen Anstrengung.

Ebenfalls anstrengend ist die für eine konstruktive Diskussion erforderliche Offenheit, sich auch inhaltlich mit geäußerten Gegenpositionen zu befassen, siehe These 7.

---

#### ⑦ Für die Verbesserung der digitalen Diskussionskultur braucht es einen Kulturwandel, der über technische Lösungen hinausgeht. {: #these7}

Ob eine Diskussion ergebnislos abbricht oder einen produktiven Austausch von Argumenten und Perspektiven ermöglicht, hängt letztlich vom *Verhalten* der Teilnehmer:innen ab. Aktuell ist eine Kultur der pauschalen Ausgrenzung und demonstrativen Nichtbefassung mit Argumenten, welche die eigene Position infrage stellen, relativ weit verbreitet.

Im digitalen Raum wird dieser Effekt oft algorithmisch verstärkt und führt zu *Echokammern*. Für konstruktive Diskussionen braucht es eine Kultur, die geprägt ist von Offenheit und Redlichkeit: Offenheit dafür, andere als die eigene Position überhaupt in Erwägung zu ziehen und Redlichkeit, um die Offenheit der anderen Seite nicht durch Scheinargumente, Scheineinwände (Stichwort [Sealioning](https://en.wikipedia.org/wiki/Sealioning)) und Grenzüberschreitungen auszunutzen.

Die Herausforderung dabei ist, dass die Festlegung des diskursiven Rahmens ("Grenzen des Sagbaren") selbst jeweils Gegenstand kontroverser Diskussionen ist, welche sich typischerweise mit der inhaltlichen Ebene überlagern ("Das wird man ja wohl noch sagen dürfen").

Grundsätzlich, d. h. aus Perspektive der reinen Logik, ist die Gültigkeit eines Arguments unabhängig davon, wer es vorgebracht hat. In der kommunikativen Praxis ist es allerdings sehr üblich, bestimmte Menschen aufgrund von tatsächlichen oder vermeintlichen Zuschreibungen von Diskursen auszuschließen.

Während sich aus der gesellschaftlichen und historischen Erfahrung sehr plausible Gründe für bestimmte Ausgrenzungsstrategien ergeben, ist zugleich die Missbrauchsanfälligkeit dieses Vorgehens offensichtlich. Anders formuliert (im Sinne Foucaults): "Wer die Grenzen des Diskurses bestimmt, bestimmt auch sein Ergebnis."

Für die Etablierung einer konstruktiven digitalen Diskussionskultur braucht es ein geschärftes Bewusstsein für dieses Dilemma und einen sehr besonnenen Umgang mit potentiellen Einschränkungen des Diskursraumes.

Die Erfahrung zeigt, dass überschaubar große Gruppen mit gegenseitigem Vertrauen bzw. aufgebauter Reputation das Problem stark abmildern. Diese Erfahrung könnte Ausgangspunkt für weitergehende Lösungsansätze sein.

---

### Perspektiven

#### ⑧ Gruppen, die es schaffen, eine konstruktive (digitale) Diskussionskultur zu etablieren, haben erhebliche Vorteile. {: #these8}


Die oben geäußerten Problemzusammenhänge (schlechte Diskussionen haben schlechte Entscheidungen zur Folge, siehe These 3) lassen sich auch anders herum betrachten: Ein konstruktives Diskussionsklima führt mittel- und langfristig zu objektiv besseren Entscheidungen. Hinzu kommt, dass Gruppenmitglieder weniger von Zeit und Kraft raubenden ergebnislosen Diskussionen frustriert werden und weniger gruppeninterne Konflikte ausbrechen.

Unter den richtigen Rahmenbedingungen wiegen diese Vorteile konstruktiver Diskussionen ihren zusätzlichen Aufwand mehr als auf.


---

#### ⑨ Anreizstrukturen und Eigenschaften von digitalen Kommunikationsplattformen lassen sich (um)gestalten. {: #these9}

Die Art, wie Kommunikationsplattformen funktionieren, und das daraus resultierende Verhalten der Kommunikationsteilnehmer:innen ergibt sich nicht aus Naturgesetzen, sondern wird durch Software festgelegt und ist somit sehr stark beeinflussbar.

Prominente Beispiele dafür, dass technische Eigenschaften starken positiven Einfluss auf das Kommunikationsverhalten haben, sind *Stackoverflow* und *Mastodon*.

*Stackoverflow* hat u. a. durch das Vergeben von Reputationspunkten und einer darauf basierenden Anzeigereihenfolge dafür gesorgt, dass sich Antworten auf technische Fragen im Bereich Softwareentwicklung viel schneller finden lassen, als es in den vorher üblichen chronologisch sortierten Diskussionsforen der Fall war.

Die Micro-Blogging-Software *Mastodon* bzw. das zugrunde liegende Protokoll ActivityPub wiederum haben u. a. durch ihren föderierten Ansatz dafür gesorgt, dass ein lebendiges Ökosystem von sog. Instanzen entsteht, die individuell entscheiden können, mit welchen anderen Instanzen Nachrichten ausgetauscht werden und mit welchen nicht. Das hat zur Folge, dass sich in diesem Netzwerk (dem *Fediverse*) ganz andere Anreizstrukturen, Moderationspraktiken und Kommunikationsmuster herausgebildet haben als beispielsweise auf X (Twitter). Eine ganz konkrete Folge ist, dass das Problem von Hassrede und Bedrohung im Fediverse wesentlich geringer ist.

Diese Beispiele zeigen, dass die technischen Eigenschaften die Art der Kommunikation erheblich beeinflussen können. Sie zeigen aber auch, dass sie mit ihrem aktuellen Stand noch weit davon entfernt sind, konstruktive Diskussionen wirklich gut zu unterstützen.

---


#### ⑩ Es gibt bereits eine Reihe technischer und sozialer Ansätze zur Unterstützung konstruktiver Diskussionen, welche aber noch getestet, verbessert und etabliert werden müssen. {: #these10}

Neben den bereits genannten Produkten (Stackoverflow, Mastodon) gab und gibt es viele softwarebasierte Ansätze, die konkret darauf abzielen, digitale Kommunikation im Sinne einer konstruktiven Diskussion zu strukturieren. Z. B.:

- [Argdown](https://argdown.org/) – Beschreibungssprache zur formalen Rekonstruktion (und Visualisierung) von Diskussionen
- [kialo.com](https://www.kialo.com) – Kommunikationsplattform, mit Fokus auf Pro- und Kontra-Argumente zu konkreten Sachfragen
- [sober-arguments.net](https://sober-arguments.net/) – Kommunikationsplattform, mit Fokus auf Pro- und Kontra-Argumente zu konkreten Sachfragen
- Weitere Projekte sind <https://github.com/arguman>, <https://socratrees.azurewebsites.net/>, <https://brabbl.com/>

Erfahrungsgemäß sind technische Ansätze zur Lösung sozialer Probleme oft gescheitert, wenn sie nicht zumindest durch soziale Ansätze ergänzt werden. Für den Bereich konstruktiver digitaler Diskussionen gibt es eine Reihe von relevanten sozialen Praktiken:

- [Street epistomology](https://streetepistemology.com/) - Gesprächstechnik für effektives gemeinsames Untersuchen von Standpunkten
- Verteilte Moderation – Auf vielen Mailinglisten übliche Praxis zur Verteilung von Macht und Aufwand
- [Code of Conduct](https://de.wikipedia.org/wiki/Verhaltenskodex) – in vielen Open-Source-Projekten übliche Vereinbarungen über gelingende Kommunikation und Zusammenarbeit


Neben diesen etablierten Praktiken sind auch soziale Innovationen denkbar, wie z. B. eine plattformübergreifende [Selbstverpflichtung](https://hedgedoc.c3d2.de/p/gubNCZPVx#/) auf konstruktive Diskussionsregeln, inklusive Kontrollmechanismen.

---

#### ⑪ Es braucht eine Bewegung, die sich mit dem Thema befasst und aktiv an pragmatischen Lösungen arbeitet, die über Appelle hinausgehen. {: #these11}

Die bisher existierenden Lösungsansätze reichen ganz offensichtlich nicht aus. Zum Teil sind sie sehr spezifisch, zum Teil sind sie noch nicht ausgereift und daher nicht massentauglich. Oder sie haben aus anderen Gründen schlicht noch keine große Reichweite.

Um die Situation zu verbessern, braucht es Menschen, die bereit sind, sich für eine konstruktive digitale Diskussionskultur zu engagieren. Und zwar nicht als Einzelkämpfer:innen, sondern in Zusammenarbeit mit anderen Menschen, die die gleichen Ziele verfolgen.
Konkret bedeutet das z. B.:

- Tools und Praktiken ausprobieren, konstruktives Feedback geben und zur iterativen Verbesserung beitragen,
- Konkrete Debatten deeskalierend begleiten, moderieren, aufarbeiten und analysieren,
- Bildungs- und Aufklärungsarbeit leisten (z. B. entsprechendes Material erstellen, Fragen beantworten, Kommentare und Leser:innenbriefe schreiben),
- Orga-Arbeit leisten,
- Infrastruktur aufbauen und betreuen.

Wir sind uns bewusst, dass sich viele Menschen an vielen Orten aus vielen Perspektiven mit dem Thema "konstruktive digitale Diskussionskultur" befassen. Wir wollen dazu beitragen, diese Menschen zu vernetzen und zu unterstützen. Wir wollen zum Aufbau einer Bewegung beitragen.

---

#### ⑫ Das Zeitfenster schließt sich. Wir müssen jetzt handeln! {: #these12}

Im historischen Vergleich sind aktuell die Bedingungen für konstruktive Diskussionen gut: Bildungsstand, Meinungs- und Pressefreiheit sind – ohne die existierenden Probleme zu verleugnen – in großen Teilen der Welt auf hohem Niveau. Allerdings zeichnen sich Gefahren ab: Einerseits gibt es erstarkende totalitäre Tendenzen auch in formal demokratischen Ländern. Andererseits geraten Gesellschaften unter Stress, z. B. durch Klimaveränderungen, durch Konflikte und Turbulenzen in der Weltwirtschaft.

Konstruktive Diskussionen können Gruppen und Gesellschaften helfen, mit Stress fertig zu werden, weil sie helfen objektive Probleme kollaborativ zu lösen.

Andererseits ist eine Kultur der rationalen und kritischen Abwägung von Argumenten unvereinbar mit totalitären Machtansprüchen. Wenn sie in deliberativen Demokratien ganz offenbar schon schwierig zu etablieren ist, dann sind die Chancen unter repressiven Bedingungen noch viel schlechter.

Die Dringlichkeit, auf konstruktive digitale Diskussionskultur hinzuarbeiten hat also zwei Gründe: Erstens, weil sie uns helfen wird, die objektiven großen Herausforderungen der kommenden Jahre viel besser zu bewältigen, als die aktuell verbreiteten dysfunktionalen und oft unwürdigen Diskurse. Und zweitens, weil eine wechselseitige Abhängigkeit zwischen der freiheitlich-demokratischen Gesellschaftsordnung und einer konstruktiven Diskussionskultur besteht.

Es ist also im ureigensten Interesse einer modernen demokratischen Gesellschaft, auch und gerade im digitalen Raum eine konstruktive Diskussionskultur zu etablieren.


### Nachbemerkung {: #nachbemerkung}

Mit diesem Positionspapier wollen wir dem Thema Diskussionskultur im digitalen Raum mehr Aufmerksamkeit verschaffen. Wir sind uns bewusst, dass unsere Thesen nicht die einzige Sichtweise auf die behandelten Probleme sind. Ganz im Sinne unseres Ziels freuen wir uns daher über Feedback und natürlich auch über Gegenthesen und (konstruktive) Kritik.



## Kontakt:

- Mastodon: <https://mastodontech.de/@kddk>
- Mail: kddk {ät} uber.space
- Matrix: <https://matrix.to/#/#kddk:matrix.org>
- Signal: <https://signal.group/#CjQKINNxipM4M31gGq6YW3TbDn67_U-qcQrMm1tIeAhmJtGEEhCzAOycIwatirqmVGrFmuCo>
- Telegram: <https://t.me/+DxXEmlgu5Xw5NTAy>


---
---

## Feedback {: #feedback}


### Definition einer gelungene Diskussion

Was ist denn für euch die Definition einer gelungene Diskussion? Es kommt ja auch immer auf das Ziel einer Gruppe darauf an: Manchmal will man gemeinsam zu einer Handlung kommen und dann ist eine Form von Konsens oder mindestens Konsent notwendig. Aber ich bin mir jetzt nicht ganz sicher, welche Gruppen ihr meint. Ich hatte die Vermutung, dass es auch einfach um so lose Netzwerke wie bei Twitter geht und da fände ich schon das echte "Zuhören" und Nachvollziehen der anderen Position mit Wohlwollen einen großen Erfolg.


Antwort (von [@cark](https://social.tchncs.de/@cark)):

> Wir sehen das Problem schlechter Diskussionskultur in verschiedenen Arten von Gruppen: Geschlossene Chatgruppen von Leuten, die sich seit Jahren kennen (und eigentlich schätzen), genauso wie völlig zufällige offene Gruppen, die nichts mehr verbindet als, dass sie gemeinsam auf eine Ausgangsnachricht geantwortet haben. Die Bereitschaf, die Existenz anderer Positionen als die eigene anzuerkennen und inhaltlich zu überdenken (also "echtes Zuhören") ist überall wichitg. Alles was darüber hinaus geht, kommt auf die jeweilige Situation an. Auch in unverbindlichen offenen Gruppen *können* sich konstruktive Diskussionen ergeben und es wäre sehr wünschenswert, dass das der Normalfall wird. Realistischer ist aber, dass wir das Problem erstmal in kleineren verbindlicheren Gruppen lösen. Was heißt "lösen"? Auch das kommt darauf an. Für den allgemeinen Falls habe ich aktuell keine bessere Formulierung als: *Die Diskussion sollte ein klares Ende haben und niemand frustriert mit ihrem Verlauf sein.* Wenn die Diskussion ein konkretes Ziel hat, z. B. einen Termin für eine Verantsaltung festzulegen, dann sollte in einer gelungenen Diskussion *zusätzlich alle relevanten Ziel-Erreichungs-Optionen inklusive ihrer Vor- und Nachteile betrachtet werden*. Klingt eher banal, ist es aber in der Praxis nicht: (Beispiel: "Plenum um 17:00? Hackt es? Manche Leute müssen auch arbeiten gehen!")

---

### Feedback vom Fediplomacy-Team {: #feedback-fediplomacy}

Hallo KDDK! Wir haben in unserem kleine Projektteam (@fediplomacy@mastodon.pwei.at) trefflich über Euer Thesenpapier diskutiert, für dessen Veröffentlichung wir danken und dessen angestoßene Initiative wir unterstützen. Im Folgenden haben wir unsere wichtigsten Punkte zusammengefasst -- bitte lest diese als Einladung, im Gespräch zu bleiben und einander gegenseitig auf gute Ideen zu bringen.

Vorweg noch ein Wort zu unserem eigenen Kontext: Wir forschen hier an der Uni Wien im Bereich Alternative Soziale Netze, schreiben als Projektteam an Förderanträgen, und tragen auch hie und da Kleinigkeiten in Form von Postings und Code zum Fediverse bei. Unser Projektuntertitel lautet "produktiver Konflikt im Fediverse" und schaut weniger auf einzelne Auseinandersetzungen, sondern auf Werkzeuge, die v.a. Mods und Admins unterstützen sollen, Konfliktsituationen auf eine Art miteinander zu bewältigen, dass der Zusammenhang des Netzes erhalten bleibt -- kein "Zerstreiten", eher "Zusammenraufen", basierend auf gegenseitigem Verständnis, das wir (auch mit technischen Mitteln) fördern wollen. Im Zusammenhang mit Eurem Thesenpapier formuliert: Wir interessieren uns weniger für die Frage der Lösung von Diskussionen, sondern vor allem für die Bedingungen der Möglichkeit bzw. für die verschiedensten Rahmenbedingungen eines Diskurses oder auch Streits. Dabei steht für uns besonders die Frage nach Moderation im Fokus.

Was uns im Thesenpapier am deutlichsten aufgefallen ist, ist erstens der Fokus auf "Diskussion", "Konsens"/"Entscheidung"/"Ergebnis", "lösen"/"Lösung", "Argument", "überzeugen". Das impliziert, dass es um eine Art Zielrichtung geht, weniger um ein Gespräch oder einen Austausch um des Austauschs Willen (wiewohl "Offenheit" für "Gegenpositionen" und "Austausch von Wissen und Perspektiven" erwähnt werden). Zweitens bemerkt haben wir, dass das skizzierte Feld von "Gruppen" über "Gesellschaft" bis "Demokratie" reicht. Ihr erwähnt dazu auch das Stichwort "Ausgrenzung" sowie Möglichkeiten und Problemen von verschiedenen Konstellationen ("Anonymität", öffentliche vs private Plattformen, Gruppen mit gewissen Eigenschaften und Verhaltensweisen).

Unsere Kritik in der Zusammenschau der beiden Punkte ist, dass wir bei beiden ein Fokusproblem sehen. Bei Punkt eins sehr eng: Austausch ist eben *nicht* notwendigerweise auf ein konkretes Ziel hin ausgerichtet, Probleme sind selten "objektiv" und klar abgegrenzt. Damit passen aber auch Zugänge, die auf *Lösungen* abzielen, nicht in Gesprächsverläufe, bei denen nicht unbedingt Probleme gelöst werden (können oder wollen). Konstruktive Diskussionskultur kann in solchen Fällen auch sein, sich nur auf die Position der anderen Seite einzulassen, ohne sich zu einer "Antwort" verpflichtet zu fühlen -- was Ihr selbstverständlich nicht ausschließt, aber es erscheint uns im Gegensatz zur Lösungsorientierung unterbetont.

Bei Punkt zwei ist der Fokus sehr weit in der Frage, um welche Konstellationen von Menschen es geht, die sich da austauschen. "Gruppen" (These 8) sind etwas anderes als Teilnehmende in einem öffentlichen Chat oder Diskussionsforum (These 1). An anderen Stellen kommt Teilhabe (These 3) bzw. auch deren Widerspruch (These 5) ins Spiel, sowie Verhaltensweisen (Thesen 4 & 7), die in kleinen Gruppen von gut miteinander bekannten Menschen ganz andere sind als in großen Gruppen oder der Öffentlichkeit. Das ist ein Widerspruch, den Ihr, wie wir finden, ohnehin selbst schon benennt. Über die Thesen hinweg entsteht aber bei uns der Eindruck, Motivationen und Verhaltensweisen und Lösungen für eine Gruppe(ngröße) werden als passend für andere Gruppen(größen) gesehen -- was potenziell nicht gemeint ist, und was wir inhaltlich nicht zutreffend fänden.

Wir denken, wie ihr auch an manchen Stellen erwähnt, dass die Diskursregeln nicht in allen Bereichen, Gruppen und Kontexten gleich sind oder sein sollten. Daraus ergibt sich die Frage, aus welchen Konstellationen können wir etwas für digitale Diskussions- und Streitkultur lernen, bzw. welche Kontexte sind zu different.

Danke für euren Vorstoß, die Initiative, die Aufmerksamkeit auf dieses Thema zu lenken, und hier auch Ideen, Beispiele und Methoden zu sammeln, wie es besser gehen kann.

Schöne Grüße vom
  Fediplomacy-Team

<br>
<br>
Vorläufige Antwort (von [@cark](https://social.tchncs.de/@cark)):

> Vielen Dank für Eure ausführliche Analyse. Wir lassen das jetzt einfach mal so stehen. Es muss ja, wie ihr richtig sagt, auch nicht immer eine *Lösung* geben. 😉
