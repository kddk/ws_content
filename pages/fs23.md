Title: Fireshonks 2023 Vortrag
Date: 2023-12-28
Author: kddk
save_as: fs23.html
Template: page
og_description: Material zum Vortrag: Konstruktive digitale Diskussionskultur – Warum sind digitale Diskussionen so frustrierend und wie können sie besser werden?
image: /img/2023_fireshonks-talk-screenshot.jpg



# Material zum Vortrag

**»Konstruktive digitale Diskussionskultur – Warum sind digitale Diskussionen so frustrierend und wie können sie besser werden?«**

 <ul>
        <li>
            <a href="https://pretalx.c3voc.de/fireshonks23/talk/ZKKHSD/">Fahrplan</a>
        </li>
        <li>
            <a href="https://media.ccc.de/v/37c3-57963-konstruktive-digitale-diskussionskultur-kddk#t=172">
            Video:<br><br>
            <img src="/img/2023_fireshonks-talk-screenshot.jpg" width="80%">
            </a>
            <br>
            <br>
        </li>
        <li>
            <a href="https://hedgedoc.c3d2.de/p/KwqPns0Py#/">
            Folien
            </a>
        </li>
        <li>
            <a href="/positionspapier.html">
            KDDK Positionspapier (Basis des Vortrages)
            </a>
        </li>
</ul>


